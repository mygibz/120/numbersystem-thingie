﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;


namespace ViewModel
{
    public class Controller : ModelBase
    {
        private int _value;
        public int dec { get => _value; set { _value = value; sourceChanged("dec"); } }
        public string bin { get => Convert.ToString(_value, 2); set { _value = Convert.ToInt32(value, 2); sourceChanged("bin"); } }
        public string oct { get => Convert.ToString(_value, 8); set { _value = Convert.ToInt32(value, 8); sourceChanged("oct"); } }
        public string hex { get => Convert.ToString(_value, 16); set { _value = Convert.ToInt32(value, 16); sourceChanged("hex"); } }
        private void sourceChanged(string source)
        {
            string[] methods = {"dec", "bin", "oct", "hex"};
            foreach (string method in methods)
            {
                if (method != source)
                    OnPropertyChanged(method);
            }
        }
    }

    public class ModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }

}
