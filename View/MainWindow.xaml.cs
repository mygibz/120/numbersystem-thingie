﻿using System.Windows;
using ViewModel;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public Controller controller;

        public MainWindow()
        {
            InitializeComponent();
            controller = new Controller();
            DataContext = controller;
        }
    }
}
